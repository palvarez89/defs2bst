#!/usr/bin/env python3
#
#  Copyright (C) 2017 Codethink Limited
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Tristan Van Berkom <tristan.vanberkom@codethink.co.uk>

import os
import re
import argparse

# Use buildstream internals
from buildstream import _yaml

class DefKind():
    CHUNK = 'chunk'
    STRATUM = 'stratum'
    SYSTEM = 'stratum'

class BstKind():
    STACK = 'stack'
    IMPORT = 'import'
    MANUAL = 'manual'
    AUTOTOOLS = 'autotools'
    DISTUTILS = 'distutils'
    MAKEMAKER = 'makemaker'
    MODBUILD = 'modulebuild'
    CMAKE = 'cmake'
    QMAKE = 'qmake'

class BuildSys():
    MANUAL = 'manual'
    MANUAL_SPLIT = 'manual-split'
    AUTOTOOLS = 'autotools'
    PYTHON = 'python-distutils'
    PYTHON3 = 'python3-distutils'
    CPAN = 'cpan'
    MODBUILD = 'module-build'
    CMAKE = 'cmake'
    QMAKE = 'qmake'


class Symbol():
    KIND = 'kind'
    DESC = 'description'
    CONTENTS = 'contents'
    BUILD_DEPENDS = 'build-depends'
    BUILDSYS = 'build-system'
    DEPENDS = 'depends'

def node_items(node):
    for key, value in node.items():
        if key == _yaml.PROVENANCE_KEY:
            continue
        yield (key, value)

# The 'contents' of a stratum is expressed as:
#
# contents:
# - stata/foo: []
# - stata/bar: []
#
# This function returns 'stata/foo' from an element above
# so we can fetch the acutal declaration from it.
def content_list_extract_name(node):
    name = None
    for key, value in node_items(node):
        assert(name is None)
        name = key
    return name


# Creates a filename for the BuildStream output of a definitions file
#
def output_name(name):
    return re.sub("^strata", "elements", name) + '.bst'

# Ensure the directory where we're going to dump something exists,
# return the joined path while we're at it
#
def ensure_directory(name, directory):
    fullpath = os.path.join(directory, name)
    fulldir = os.path.dirname(fullpath)
    os.makedirs(fulldir, exist_ok=True)
    return fullpath

def chunk_kind(node):
    kind = _yaml.node_get(node, str, Symbol.BUILDSYS, 'manual')
    

def convert_chunk(defs, name, node, directory, base_deps):
    outname = output_name(name)
    fullpath = ensure_directory(outname, directory)
    print("Converting Chunk {} -> {}".format(name, fullpath))



def convert_stratum(defs, name, node, directory):
    outname = output_name(name)
    fullpath = ensure_directory(outname, directory)
    print("Converting Stratum: {} -> {}".format(name, outname))

    # The stratum dependencies are transfered to the chunks included in
    # this stratum
    base_deps = _yaml.node_get(node, list, Symbol.BUILD_DEPENDS, default_value=[])
    base_deps = [output_name(dep) for dep in base_deps]

    depends = []
    contents = _yaml.node_get(node, list, Symbol.CONTENTS, default_value=[])
    for thing in contents:
        chunkname = content_list_extract_name(thing)
        chunk = defs.get(chunkname)
        depends.append(output_name(chunkname))
        convert_chunk(defs, chunkname, chunk, directory, base_deps)

    # Converting stacks to elements is not very difficult
    description = node.get(Symbol.DESC)
    stack = {}
    stack[Symbol.KIND] = BstKind.STACK
    stack[Symbol.DEPENDS] = depends
    if description is not None:
        stack[Symbol.DESC] = description
    _yaml.dump(stack, fullpath)

def convert(defs, directory):
    os.makedirs(directory, exist_ok=True)
    defs = _yaml.load(defs)

    # Loop over stratum, convert one stratum at a time, skipping build-essential
    for key, value in node_items(defs):
        kind = _yaml.node_get(value, str, Symbol.KIND, default_value=DefKind.CHUNK)
        if key != 'strata/build-essential' and kind == DefKind.STRATUM:
            convert_stratum(defs, key, value, directory)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument ('defs', help="A normalized yaml file built by YBD")
    parser.add_argument ('output', help="A directory for dumping the converted project")

    args = parser.parse_args()

    convert(args.defs, args.output)

